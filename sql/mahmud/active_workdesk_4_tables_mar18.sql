-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2019 at 11:43 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `active_workdesk`
--

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `client_id` bigint(20) NOT NULL,
  `member_profile_id` varchar(25) NOT NULL,
  `status` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `gender` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `is_closed` varchar(20) NOT NULL,
  `date_of_birth` varchar(50) NOT NULL,
  `height` decimal(10,2) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `profile_image` mediumtext NOT NULL,
  `introduction` longtext NOT NULL,
  `basic_info` longtext NOT NULL,
  `present_address` longtext NOT NULL,
  `education_and_career` longtext NOT NULL,
  `physical_attributes` longtext NOT NULL,
  `language` longtext NOT NULL,
  `hobbies_and_interest` longtext NOT NULL,
  `personal_attitude_and_behavior` longtext NOT NULL,
  `residency_information` longtext NOT NULL,
  `spiritual_and_social_background` longtext NOT NULL,
  `life_style` longtext NOT NULL,
  `astronomic_information` longtext NOT NULL,
  `permanent_address` longtext NOT NULL,
  `family_info` longtext NOT NULL,
  `additional_personal_details` longtext NOT NULL,
  `partner_expectation` longtext NOT NULL,
  `interest` longtext NOT NULL,
  `short_list` longtext NOT NULL,
  `followed` longtext NOT NULL,
  `ignored` longtext NOT NULL,
  `ignored_by` longtext NOT NULL,
  `gallery` longtext NOT NULL,
  `happy_story` longtext NOT NULL,
  `package_info` longtext NOT NULL,
  `payments_info` longtext NOT NULL,
  `interested_by` longtext NOT NULL,
  `follower` int(11) NOT NULL,
  `membership` int(11) NOT NULL,
  `notifications` longtext NOT NULL,
  `profile_status` int(11) NOT NULL,
  `member_since` datetime NOT NULL,
  `daily_job_post_count` int(11) NOT NULL,
  `weekly_job_post_count` int(11) NOT NULL,
  `monthly_job_post_count` int(11) NOT NULL,
  `profile_completion` int(11) NOT NULL,
  `is_blocked` varchar(20) NOT NULL,
  `privacy_status` longtext NOT NULL,
  `pic_privacy` longtext NOT NULL,
  `report_profile` longtext,
  `reported_by` int(11) DEFAULT '0',
  `plan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`client_id`, `member_profile_id`, `status`, `first_name`, `last_name`, `gender`, `email`, `mobile`, `is_closed`, `date_of_birth`, `height`, `password`, `profile_image`, `introduction`, `basic_info`, `present_address`, `education_and_career`, `physical_attributes`, `language`, `hobbies_and_interest`, `personal_attitude_and_behavior`, `residency_information`, `spiritual_and_social_background`, `life_style`, `astronomic_information`, `permanent_address`, `family_info`, `additional_personal_details`, `partner_expectation`, `interest`, `short_list`, `followed`, `ignored`, `ignored_by`, `gallery`, `happy_story`, `package_info`, `payments_info`, `interested_by`, `follower`, `membership`, `notifications`, `profile_status`, `member_since`, `daily_job_post_count`, `weekly_job_post_count`, `monthly_job_post_count`, `profile_completion`, `is_blocked`, `privacy_status`, `pic_privacy`, `report_profile`, `reported_by`, `plan`) VALUES
(1, '8BAFAB5C1', 'approved', 'Demo', 'X', 0, 'demoemailacc.xclient@gmail.com', '1321321', 'no', '0', NULL, '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '[{\"profile_image\":\"default.jpg\",\"thumb\":\"default_thumb.jpg\"}]', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '[]', '[]', '[]', '[]', '', '', '[{\"current_package\":\"Client Default\",\"package_price\":\"0\",\"payment_type\":\"None\"}]', '[]', '[]', 0, 1, '[]', 1, '2019-03-18 11:03:12', 5, 6, 0, 0, 'no', '', '', '[]', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `client_plan`
--

CREATE TABLE `client_plan` (
  `client_plan_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `daily_job_post_count` int(11) NOT NULL,
  `weekly_job_post_count` int(11) NOT NULL,
  `monthly_job_post_count` int(11) NOT NULL,
  `image` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_plan`
--

INSERT INTO `client_plan` (`client_plan_id`, `name`, `amount`, `daily_job_post_count`, `weekly_job_post_count`, `monthly_job_post_count`, `image`) VALUES
(1, 'Client Default', '0', 5, 6, 3, '[{\"image\":\"plan_1.png\",\"thumb\":\"plan_1_thumb.png\"}]'),
(2, 'Client Bronzes', '14', 14, 13, 3, '[{\"image\":\"plan_2.png\",\"thumb\":\"plan_2_thumb.png\"}]'),
(3, 'Client Silver', '25', 15, 10, 5, '[{\"image\":\"plan_3.png\",\"thumb\":\"plan_3_thumb.png\"}]'),
(4, 'Client Gold', '35', 20, 15, 5, '[{\"image\":\"plan_4.png\",\"thumb\":\"plan_4_thumb.png\"}]'),
(5, 'Client Platinum', '45', 25, 20, 7, '[{\"image\":\"plan_5.png\",\"thumb\":\"plan_5_thumb.png\"}]'),
(9, 'ss', '44', 444, 44, 44, '[{\"image\":\"client_plan_9.png\",\"thumb\":\"client_plan_9_thumb.png\"}]'),
(10, 'gg', '33', 33, 1, 333, '[{\"image\":\"client_plan_10.png\",\"thumb\":\"client_plan_10_thumb.png\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `freelancer`
--

CREATE TABLE `freelancer` (
  `freelancer_id` bigint(20) NOT NULL,
  `member_profile_id` varchar(25) NOT NULL,
  `status` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `gender` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `is_closed` varchar(20) NOT NULL,
  `date_of_birth` varchar(50) NOT NULL,
  `height` decimal(10,2) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `profile_image` mediumtext NOT NULL,
  `introduction` longtext NOT NULL,
  `basic_info` longtext NOT NULL,
  `present_address` longtext NOT NULL,
  `education_and_career` longtext NOT NULL,
  `physical_attributes` longtext NOT NULL,
  `language` longtext NOT NULL,
  `hobbies_and_interest` longtext NOT NULL,
  `personal_attitude_and_behavior` longtext NOT NULL,
  `residency_information` longtext NOT NULL,
  `spiritual_and_social_background` longtext NOT NULL,
  `life_style` longtext NOT NULL,
  `astronomic_information` longtext NOT NULL,
  `permanent_address` longtext NOT NULL,
  `family_info` longtext NOT NULL,
  `additional_personal_details` longtext NOT NULL,
  `partner_expectation` longtext NOT NULL,
  `interest` longtext NOT NULL,
  `short_list` longtext NOT NULL,
  `followed` longtext NOT NULL,
  `ignored` longtext NOT NULL,
  `ignored_by` longtext NOT NULL,
  `gallery` longtext NOT NULL,
  `happy_story` longtext NOT NULL,
  `package_info` longtext NOT NULL,
  `payments_info` longtext NOT NULL,
  `interested_by` longtext NOT NULL,
  `follower` int(11) NOT NULL,
  `membership` int(11) NOT NULL,
  `notifications` longtext NOT NULL,
  `profile_status` int(11) NOT NULL,
  `member_since` datetime NOT NULL,
  `daily_bidding_count` int(11) NOT NULL,
  `weekly_bidding_count` int(11) NOT NULL,
  `monthly_bidding_count` int(11) NOT NULL,
  `profile_completion` int(11) NOT NULL,
  `is_blocked` varchar(20) NOT NULL,
  `privacy_status` longtext NOT NULL,
  `pic_privacy` longtext NOT NULL,
  `report_profile` longtext,
  `reported_by` int(11) DEFAULT '0',
  `plan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `freelancer`
--

INSERT INTO `freelancer` (`freelancer_id`, `member_profile_id`, `status`, `first_name`, `last_name`, `gender`, `email`, `mobile`, `is_closed`, `date_of_birth`, `height`, `password`, `profile_image`, `introduction`, `basic_info`, `present_address`, `education_and_career`, `physical_attributes`, `language`, `hobbies_and_interest`, `personal_attitude_and_behavior`, `residency_information`, `spiritual_and_social_background`, `life_style`, `astronomic_information`, `permanent_address`, `family_info`, `additional_personal_details`, `partner_expectation`, `interest`, `short_list`, `followed`, `ignored`, `ignored_by`, `gallery`, `happy_story`, `package_info`, `payments_info`, `interested_by`, `follower`, `membership`, `notifications`, `profile_status`, `member_since`, `daily_bidding_count`, `weekly_bidding_count`, `monthly_bidding_count`, `profile_completion`, `is_blocked`, `privacy_status`, `pic_privacy`, `report_profile`, `reported_by`, `plan`) VALUES
(1, 'D90DFB471', 'approved', 'Hridoy', 'Mahmud', 1, 'hridoymahmud71@gmail.com', '1513215', 'no', '551484000', '0.00', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '[{\"profile_image\":\"default.jpg\",\"thumb\":\"default_thumb.jpg\"}]', 'Aello, dis is new freelancer.\r\nC#, SQL Server 2008', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '[]', '[]', '[]', '[]', '[]', '[]', '', '[{\"current_package\":\"Freelancer Default\",\"package_price\":\"0\",\"payment_type\":\"By Admin\"}]', '[]', '[]', 0, 1, '[]', 1, '2019-03-12 11:03:41', 25, 21, 8, 0, 'no', '', '', '[]', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `freelancer_plan`
--

CREATE TABLE `freelancer_plan` (
  `freelancer_plan_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `daily_bidding_count` int(11) NOT NULL,
  `weekly_bidding_count` int(11) NOT NULL,
  `monthly_bidding_count` int(11) NOT NULL,
  `image` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `freelancer_plan`
--

INSERT INTO `freelancer_plan` (`freelancer_plan_id`, `name`, `amount`, `daily_bidding_count`, `weekly_bidding_count`, `monthly_bidding_count`, `image`) VALUES
(1, 'Freelancer Default', '0', 5, 6, 3, '[{\"image\":\"plan_1.png\",\"thumb\":\"plan_1_thumb.png\"}]'),
(2, 'Freelancer Bronze', '15', 10, 10, 5, '[{\"image\":\"plan_2.png\",\"thumb\":\"plan_2_thumb.png\"}]'),
(3, 'Freelancer Silver', '25', 15, 10, 5, '[{\"image\":\"plan_3.png\",\"thumb\":\"plan_3_thumb.png\"}]'),
(4, 'Freelancer Gold', '35', 20, 15, 5, '[{\"image\":\"plan_4.png\",\"thumb\":\"plan_4_thumb.png\"}]'),
(5, 'Freelancer Platinum', '45', 25, 20, 7, '[{\"image\":\"plan_5.png\",\"thumb\":\"plan_5_thumb.png\"}]'),
(7, 'dd', '33', 33, 24, 31, '[{\"image\":\"freelancer_plan_7.png\",\"thumb\":\"freelancer_plan_7_thumb.png\"}]');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `client_plan`
--
ALTER TABLE `client_plan`
  ADD PRIMARY KEY (`client_plan_id`);

--
-- Indexes for table `freelancer`
--
ALTER TABLE `freelancer`
  ADD PRIMARY KEY (`freelancer_id`);

--
-- Indexes for table `freelancer_plan`
--
ALTER TABLE `freelancer_plan`
  ADD PRIMARY KEY (`freelancer_plan_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `client_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `client_plan`
--
ALTER TABLE `client_plan`
  MODIFY `client_plan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `freelancer`
--
ALTER TABLE `freelancer`
  MODIFY `freelancer_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `freelancer_plan`
--
ALTER TABLE `freelancer_plan`
  MODIFY `freelancer_plan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
