<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<div id="page-head">
		<!--Page Title-->
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<div id="page-title">
			<h1 class="page-header text-overflow"><?php echo translate('freelancers')?></h1>
			<!--Searchbox-->
			<div class="searchbox">
				<div class="pull-right">
					<a href="<?=base_url()?>admin/freelancers/<?=$parameter?>" class="btn btn-danger btn-sm btn-labeled fa fa-step-backward" type="submit"><?php echo translate('go_back')?></a>
				</div>
			</div>
		</div>
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<!--End page title-->
		<!--Breadcrumb-->
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<ol class="breadcrumb">
			<li><a href="#"><?php echo translate('home')?></a></li>
			<li><a href="#"><?php echo translate('freelancers')?></a></li>
			<li><a href="#"><?=translate($member_type)?> <?php echo translate('freelancers')?></a></li>
			<li class="active"><?php echo translate('freelancer_details')?></li>
		</ol>
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<!--End breadcrumb-->
	</div>
	<!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    
					<div class="fixed-fluid">
					<?php 
						$freelancers = array();
						if ($member_type == "Free") {
							$member = $get_free_freelancer_by_id;
						}
						elseif ($member_type == "Premium") {
							$member = $get_premium_freelancer_by_id;
						}
					?>
						<?php
							foreach ($member as $value) {
								$image = json_decode($value->profile_image, true);
								$basic_info = json_decode($value->basic_info, true);
								$permanent_address = json_decode($value->permanent_address, true);
								$language = json_decode($value->language, true);
							}
							include_once "left_panel.php";
							include_once "freelancer_info.php";
						?>
					</div>					
                </div>
                <!--===================================================-->
                <!--End page content-->
</div>

<!--Default Bootstrap Modal-->
<!--===================================================-->
<div class="modal fade" id="package_modal" role="dialog" tabindex="-1" aria-labelledby="package_modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--Modal header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                <h4 class="modal-title"><?php echo translate('package_information')?></h4>
            </div>
           	<!--Modal body-->
            <div class="modal-body" id="package_modal_body">
            	
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="block_modal" role="dialog" tabindex="-1" aria-labelledby="block_modal" aria-hidden="true">
    <div class="modal-dialog" style="width: 400px;">
        <div class="modal-content">
            <!--Modal header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                <h4 class="modal-title"><?php echo translate('confirm_your_action')?></h4>
            </div>
           	<!--Modal body-->
            <div class="modal-body">
            	<p><?php echo translate('are_you_sure_you_want_to')?> "<b id="block_type"></b>" <?php echo translate('this_user?')?>?</p>
            	<div class="text-right">
            		<input type="hidden" id="freelancer_id" name="freelancer_id" value="">
            		<button data-dismiss="modal" class="btn btn-default btn-sm" type="button" id="modal_close"><?php echo translate('close')?></button>
                	<button class="btn btn-primary btn-sm" id="block_status" value=""><?php echo translate('confirm')?></button>
            	</div>
            </div>
        </div>
    </div>
</div>
<!--===================================================-->
<!--End Default Bootstrap Modal-->
<!--Default Bootstrap Modal-->
<!--===================================================-->
<div class="modal fade" id="upgrade_modal" role="dialog" tabindex="-1" aria-labelledby="upgrade_modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--Modal header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                <h4 class="modal-title"><?php echo translate('upgrade_package')?></h4>
            </div>
           	<!--Modal body-->
            <div class="modal-body">
            	<form class="form-horizontal" id="package_upgrade_form" method="POST" action="<?=base_url()?>admin/freelancers/upgrade_freelancer_package">
					<div class="row">
					    <div class="col-md-10 col-md-offset-1">
					        <div class="upgrade">
					            <h5><?php echo translate('choose_your_package')?></h5>
					            <div class="text-left">
					                <div class="px-2 py-2">
					                	<input type="hidden" id="up_freelancer_id" name="up_freelancer_id" value="">
					                	<input type="hidden" id="member_type" name="member_type" value="<?=$parameter?>">
					                    <?php echo $this->Crud_model->select_html('plan', 'plan', 'name', 'add', 'form-control form-control-sm selectpicker', '', '', '', '');?>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
	            	<div class="text-center" style="margin-top: 15px;">
		        		<button class="btn btn-success add-tooltip" type="submit"><?=translate('submit')?></button>
		        	</div>
		        </form>         	
            </div>
        </div>
    </div>
</div>
<!--===================================================-->
<!--End Default Bootstrap Modal-->
<script>
	function view_package(id){
		$.ajax({
		    url: "<?=base_url()?>admin/freelancer_package_modal/"+id,
		    success: function(response) {
				$("#package_modal_body").html(response);
		    },
			fail: function (error) {
			    alert(error);
			}
		});
	}
</script>
<script>
	function block(status, member_id){
	    $("#block_status").val(status);
	    if (status == 'yes') {
	    	$("#block_type").html("<?php echo translate('unblock')?>");
	    }
	    if (status == 'no') {
			$("#block_type").html("<?php echo translate('block')?>");
	    }
	    $("#freelancer_id").val(member_id);
	}

	$("#block_status").click(function(){
    	$.ajax({
		    url: "<?=base_url()?>admin/freelancer_block/"+$("#block_status").val()+"/"+$("#freelancer_id").val(),
		    success: function(response) {
		    	// alert(response);
				window.location.href = "<?=base_url()?>admin/freelancers/<?=$parameter?>";
		    },
			fail: function (error) {
			    alert(error);
			}
		});
    })
</script>