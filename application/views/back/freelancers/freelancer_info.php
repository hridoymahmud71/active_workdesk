<div class="fluid">
	<div class="fixed-fluid">
		<div class="fluid">
			<div class="panel">
				<div class="panel-body">

					<!--Dark Panel-->
			        <!--===================================================-->
			        <div class="pull-right">
						<button data-target='#delete_modal' data-toggle='modal' class='btn btn-danger btn-sm btn-labeled fa fa-trash' data-toggle='tooltip' data-placement='top' title='".translate('delete_freelancer')."' onclick='delete_freelancer("<?=$value->freelancer_id?>")'><?php echo translate('delete')?></i></button>
						<a href="<?=base_url()?>admin/freelancers/<?=$parameter?>/edit_freelancer/<?=$value->freelancer_id?>" class="btn btn-primary btn-sm btn-labeled fa fa-edit" type="button"><?php echo translate('edit')?></a>
					</div>

			        <div class="text-left">
			        	<h4>Freelancer ID - <?=$value->member_profile_id?></h4>
			        </div>

			        <div class="panel panel-dark">
			            <div class="panel-heading">
			                <h3 class="panel-title"><?php echo translate('introduction')?></h3>
			            </div>
			            <div class="panel-body">
			                <p><?=$value->introduction?></p>
			            </div>
			        </div>

			        <div class="panel panel-dark">
			            <div class="panel-heading">
			                <h3 class="panel-title"><?php echo translate('basic_information')?></h3>
			            </div>
			            <div class="panel-body">
			                <table class="table table-condenced">
							<tr>
								<td>
									<b><?php echo translate('first_name')?></b>
								</td>
								<td>
									<?=$value->first_name?>
								</td>
								<td>
									<b><?php echo translate('last_name')?></b>
								</td>
								<td>
									<?=$value->last_name?>
								</td>
							</tr>
							<tr>
								<td>
									<b><?php echo translate('gender')?></b>
								</td>
								<td>
                            		<?=$this->Crud_model->get_type_name_by_id('gender', $value->gender)?>
								</td>
								<td>
									<b><?php echo translate('email')?></b>
								</td>
								<td>
									<?=$value->email?>
								</td>
							</tr>
							<tr>
								<td>
									<b><?php echo translate('age')?></b>
								</td>
								<td>
									<?=$calculated_age = (date('Y') - date('Y', $value->date_of_birth));?>
								</td>
							</tr>
							<tr>
								<td>
									<b><?php echo translate('mobile')?></b>
								</td>
								<td>
									<?=$value->mobile?>
								</td>
								<td>
									 <?=$this->Crud_model->get_type_name_by_id('on_behalf', $basic_info[0]['on_behalf']);?>
								</td>
							</tr>
							<tr>
								<td>
									<b><?php echo translate('date_of_birth')?></b>
								</td>
								<td>
									<?=date('d/m/Y', $value->date_of_birth)?>
								</td>
								<td>
									<b></b>
								</td>
								<td>

								</td>
							</tr>
							</table>
			            </div>
			        </div>
			        <?php
                        if ($this->db->get_where('frontend_settings', array('type' => 'present_address'))->row()->value == "yes") {
                    ?>
			        <div class="panel panel-dark">
			            <div class="panel-heading">
			                <h3 class="panel-title"><?php echo translate('present_address')?></h3>
			            </div>
			            <div class="panel-body">
			                <table class="table">
							<tr>
								<td>
									<b><?php echo translate('country')?></b>
								</td>
								<td>
                            		<?=$this->Crud_model->get_type_name_by_id('country', $present_address[0]['country']);?>
								</td>
								<td>
									<b><?php echo translate('state')?></b>
								</td>
								<td>
									<?=$this->Crud_model->get_type_name_by_id('state', $present_address[0]['state']);?>
								</td>
							</tr>
							<tr>
								<td>
									<b><?php echo translate('city')?></b>
								</td>
								<td>
									<?=$this->Crud_model->get_type_name_by_id('city', $present_address[0]['city']);?>
								</td>
								<td>
									<b><?php echo translate('postal-Code')?></b>
								</td>
								<td>
									<?=$present_address[0]['postal_code']?>
								</td>
							</tr>
							</table>
			            </div>
			        </div>
			        <?php
                        }
                    ?>
                    <?php
                        if ($this->db->get_where('frontend_settings', array('type' => 'language'))->row()->value == "yes") {
                    ?>
			        <div class="panel panel-dark">
			            <div class="panel-heading">
			                <h3 class="panel-title"><?php echo translate('language')?></h3>
			            </div>
			            <div class="panel-body">
			                <table class="table">
							<tr>
								<td>
									<b><?php echo translate('mother_tongue')?></b>
								</td>
								<td>
									<?=$this->Crud_model->get_type_name_by_id('language', $language[0]['mother_tongue']);?>
								</td>
								<td>
									<b><?php echo translate('language')?></b>
								</td>
								<td>
									<?=$this->Crud_model->get_type_name_by_id('language', $language[0]['language']);?>
								</td>
							</tr>
							<tr>
								<td>
									<b><?php echo translate('speak')?></b>
								</td>
								<td>
									<?=$this->Crud_model->get_type_name_by_id('language', $language[0]['speak']);?>
								</td>
								<td>
									<b><?php echo translate('read')?></b>
								</td>
								<td>
									<?=$this->Crud_model->get_type_name_by_id('language', $language[0]['read']);?>
								</td>
							</tr>
							</table>
			            </div>
			        </div>
			        <?php
                        }
                    ?>
                    <?php
                        if ($this->db->get_where('frontend_settings', array('type' => 'permanent_address'))->row()->value == "yes") {
                    ?>
			        <div class="panel panel-dark">
			            <div class="panel-heading">
			                <h3 class="panel-title"><?php echo translate('permanent_address')?></h3>
			            </div>
			            <div class="panel-body">
			                <table class="table">
							<tr>
								<td>
									<b><?php echo translate('country')?></b>
								</td>
								<td>
									<?=$this->Crud_model->get_type_name_by_id('country', $permanent_address[0]['permanent_country']);?>
								</td>
								<td>
									<b><?php echo translate('state')?></b>
								</td>
								<td>
									<?=$this->Crud_model->get_type_name_by_id('state', $permanent_address[0]['permanent_state']);?>
								</td>
							</tr>
							<tr>
								<td>
									<b><?php echo translate('city')?></b>
								</td>
								<td>
									<?=$this->Crud_model->get_type_name_by_id('city', $permanent_address[0]['permanent_city']);?>
								</td>
								<td>
									<b><?php echo translate('postal-Code')?></b>
								</td>
								<td>
									<?=$permanent_address[0]['permanent_postal_code']?>
								</td>
							</tr>
							</table>
			            </div>
			        </div>
			        <?php
                        }
                    ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
    function delete_freelancer(id){
	    $("#delete_freelancer").val(id);
	}

	function deleteAFreelancer() {
		$.ajax({
		    url: "<?=base_url()?>admin/freelancer_delete/"+$("#delete_freelancer").val(),
		    success: function(response) {
				window.location.href = "<?=base_url()?>admin/freelancers/<?=$parameter?>";
		    },
			fail: function (error) {
			    alert(error);
			}
		});
	}
</script>

<div class="modal fade" id="delete_modal" role="dialog" tabindex="-1" aria-labelledby="delete_modal" aria-hidden="true">
    <div class="modal-dialog" style="width: 400px;">
        <div class="modal-content">
            <!--Modal header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                <h4 class="modal-title"><?php echo translate('confirm_delete')?></h4>
            </div>
           	<!--Modal body-->
            <div class="modal-body">
            	<p><?php echo translate('are_you_sure_you_want_to_delete_this_data?')?></p>
            	<div class="text-right">
            		<button data-dismiss="modal" class="btn btn-default btn-sm" type="button" id="modal_close"><?php echo translate('close')?></button>
                	<button class="btn btn-danger btn-sm" id="delete_freelancers" onclick="deleteAFreelancer()"value=""><?php echo translate('delete')?></button>
            	</div>
            </div>

        </div>
    </div>
</div>
