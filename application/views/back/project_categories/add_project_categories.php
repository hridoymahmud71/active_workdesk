<!--Horizontal Form-->
<!--===================================================-->
<form class="form-horizontal" id="project_category_form" method="post" enctype="multipart/form-data">
	<div class="panel-body">
		<div class="form-group">
			<label class="col-sm-3 control-label" for="demo-hor-inputemail"><b><?php echo translate('title')?></b></label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="title" value="" required="">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="project_category_icon"><b><?php echo translate('icon')?></b></label>
	        <div class="col-sm-6">
	            <img class="img-responsive img-border icon_blah" src="<?=base_url()?>uploads/project_category_image/default_image.jpg" style="max-width: 30%;">
	        </div>
	        <div class="col-sm-6 col-sm-offset-3" style="margin-top: 10px">
	            <span class="pull-left btn btn-dark btn-sm btn-file">
	                <?php echo translate('select_a_photo')?>
	                <input type="file" name="icon" id="project_category_icon" class="form-control icon_imgInp"  accept="image/*">
	            </span>
	        </div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="project_category_banner"><b><?php echo translate('banner')?></b></label>
	        <div class="col-sm-6">
	            <img class="img-responsive img-border banner_blah" src="<?=base_url()?>uploads/project_category_image/default_image.jpg" style="max-width: 65%;">
	        </div>
	        <div class="col-sm-6 col-sm-offset-3" style="margin-top: 10px">
	            <span class="pull-left btn btn-dark btn-sm btn-file">
	                <?php echo translate('select_a_photo')?>
	                <input type="file" name="banner" id="project_category_banner" class="form-control banner_imgInp" accept="image/*">
	            </span>
	        </div>
		</div>

	</div>
</form>
<!--===================================================-->
<!--End Horizontal Form-->

<script>
	// SCRIT FOR PROJECT CATEGORY ICON UPLOAD
    function icon_readURL_all(input) {
    	//alert();
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var parent = $(input).closest('.form-group');
            reader.onload = function (e) {
                parent.find('.wrap').hide('fast');
                parent.find('.icon_blah').attr('src', e.target.result);
                parent.find('.wrap').show('fast');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".form-horizontal").on('change', '.icon_imgInp', function () {
        icon_readURL_all(this);
    });

    // SCRIT FOR PROJECT CATEGORY ICON UPLOAD
    function banner_readURL_all(input) {
    	//alert();
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var parent = $(input).closest('.form-group');
            reader.onload = function (e) {
                parent.find('.wrap').hide('fast');
                parent.find('.banner_blah').attr('src', e.target.result);
                parent.find('.wrap').show('fast');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".form-horizontal").on('change', '.banner_imgInp', function () {
        banner_readURL_all(this);
    });

</script>