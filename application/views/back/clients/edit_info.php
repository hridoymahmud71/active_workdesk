<div class="fluid">
	<div class="fixed-fluid">
		<div class="fluid">
			<?php if (!empty(validation_errors())): ?>
                <div class="widget" id="profile_error">
                    <div style="border-bottom: 1px solid #e6e6e6;">
                        <div class="card-title" style="padding: 0.5rem 1.5rem; color: #fcfcfc; background-color: #de1b1b; border-top-right-radius:0.25rem; border-top-left-radius:0.25rem;">You <b>Must Provide</b> the Information(s) bellow</div>
                        <div class="card-body" style="padding: 0.5rem 1.5rem; background: rgba(222, 27, 27, 0.10);">
                            <style>
                                #profile_error p {
                                    color: #DE1B1B !important; margin: 0px !important; font-size: 12px !important;
                                }
                            </style>
                            <?= validation_errors();?>
                        </div>
                    </div>
                </div>
            <?php
                endif;
            ?>
			<form id="edit_profile_form" class="form-default" role="form" action="<?=base_url()?>admin/clients/update_client/<?=$value->client_id?>/<?=$parameter?>" method="POST">
				<div class="panel">
					<div class="panel-body">
						<!--Dark Panel-->
				        <!--===================================================-->
				        <div class="pull-right">
							<button class="btn btn-primary btn-sm btn-labeled fa fa-floppy-o" type="submit"><?php echo translate('update')?></button>
						</div>

				        <div class="text-left">
				        	<h4>Member ID - <?=$value->member_profile_id?></h4>
				        </div>

				        <div class="panel panel-dark">
				            <div class="panel-heading">
				                <h3 class="panel-title"><?php echo translate('introduction')?></h3>
				            </div>
				            <div class="panel-body">
				            	<textarea name="introduction" class="form-control no-resize" rows="6"><?php if(!empty($form_contents)){echo $form_contents['introduction'];} else{echo $value->introduction;}?></textarea>
				            </div>
				        </div>

				        <div class="panel panel-dark">
				            <div class="panel-heading">
				                <h3 class="panel-title"><?php echo translate('basic_information')?></h3>
				            </div>
				            <div class="panel-body">
				            	<div class='clearfix'>
	                            </div>
				                <div class="row">
	                                <div class="col-md-6">
	                                    <div class="form-group has-feedback">
	                                        <label for="first_name" class="text-uppercase c-gray-light"><?php echo translate('first_name')?><span class="text-danger">*</span></label>
	                                        <input type="text" class="form-control no-resize" name="first_name" value="<?php if(!empty($form_contents)){echo $form_contents['first_name'];} else{echo $value->first_name;}?>">
	                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                                        <div class="help-block with-errors">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-md-6">
	                                    <div class="form-group has-feedback">
	                                        <label for="last_name" class="text-uppercase c-gray-light"><?php echo translate('last_name')?><span class="text-danger">*</span></label>
	                                        <input type="text" class="form-control no-resize" name="last_name" value="<?php if(!empty($form_contents)){echo $form_contents['last_name'];} else{echo $value->last_name;}?>">
	                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                                        <div class="help-block with-errors">
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <div class="form-group has-feedback">
	                                        <label for="gender" class="text-uppercase c-gray-light"><?php echo translate('gender')?><span class="text-danger">*</span></label>
	                                        <?php
	                                            if (!empty($form_contents)) {
	                                                echo $this->Crud_model->select_html('gender', 'gender', 'name', 'edit', 'form-control form-control-sm selectpicker', $form_contents['gender'], '', '', '');
	                                            }
	                                            else {
	                                                echo $this->Crud_model->select_html('gender', 'gender', 'name', 'edit', 'form-control form-control-sm selectpicker', $value->gender, '', '', '');
	                                            }
	                                        ?>
	                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                                        <div class="help-block with-errors">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-md-6">
	                                    <div class="form-group has-feedback">
	                                        <label for="email" class="text-uppercase c-gray-light"><?php echo translate('email')?><span class="text-danger">*</span></label>
	                                        <input type="hidden" name="old_email" value="<?=$value->email?>">
	                                        <input type="email" class="form-control no-resize" name="email" value="<?php if(!empty($form_contents)){echo $form_contents['email'];} else{echo $value->email;}?>">
	                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                                        <div class="help-block with-errors">
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <div class="form-group has-feedback">
	                                        <label for="date_of_birth" class="text-uppercase c-gray-light"><?php echo translate('date_of_birth')?><span class="text-danger">*</span></label>
	                                        <input type="date" class="form-control no-resize" name="date_of_birth" value="<?php if(!empty($form_contents)){echo $form_contents['date_of_birth'];} else{echo date('Y-m-d', $value->date_of_birth);}?>">
	                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                                        <div class="help-block with-errors">
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <div class="form-group has-feedback">
	                                        <label for="mobile" class="text-uppercase c-gray-light"><?php echo translate('mobile')?><span class="text-danger">*</span></label>
	                                        <input type="hidden" name="old_mobile" value="<?=$value->mobile?>">
	                                        <input type="number" class="form-control no-resize" name="mobile" value="<?=$value->mobile?>">
	                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                                        <div class="help-block with-errors"></div>
	                                    </div>
	                                </div>
	                            </div>
				            </div>
				        </div>

	                    <?php
	                        if ($this->db->get_where('frontend_settings', array('type' => 'language'))->row()->value == "yes") {
	                    ?>
				        <div class="panel panel-dark">
				            <div class="panel-heading">
				                <h3 class="panel-title"><?php echo translate('language')?></h3>
				            </div>
				            <div class="panel-body">
				                <div class='clearfix'>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <div class="form-group has-feedback">
	                                        <label for="mother_tongue" class="text-uppercase c-gray-light"><?php echo translate('mother_tongue')?><span class="text-danger">*</span></label>
	                                        <?php
	                                            if (!empty($form_contents)) {
	                                                echo $this->Crud_model->select_html('language', 'mother_tongue', 'name', 'edit', 'form-control form-control-sm selectpicker', $form_contents['mother_tongue'], '', '', '');
	                                            }
	                                            else {
	                                                echo $this->Crud_model->select_html('language', 'mother_tongue', 'name', 'edit', 'form-control form-control-sm selectpicker', $language[0]['mother_tongue'], '', '', '');
	                                            }
	                                        ?>
	                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                                        <div class="help-block with-errors">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-md-6">
	                                    <div class="form-group has-feedback">
	                                        <label for="language" class="text-uppercase c-gray-light"><?php echo translate('language')?></label>
	                                        <?php
	                                            if (!empty($form_contents)) {
	                                                echo $this->Crud_model->select_html('language', 'language', 'name', 'edit', 'form-control form-control-sm selectpicker', $form_contents['language'], '', '', '');
	                                            }
	                                            else {
	                                                echo $this->Crud_model->select_html('language', 'language', 'name', 'edit', 'form-control form-control-sm selectpicker', $language[0]['language'], '', '', '');
	                                            }
	                                        ?>
	                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                                        <div class="help-block with-errors">
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <div class="form-group has-feedback">
	                                        <label for="speak" class="text-uppercase c-gray-light"><?php echo translate('speak')?></label>
	                                        <?php
	                                            if (!empty($form_contents)) {
	                                                echo $this->Crud_model->select_html('language', 'speak', 'name', 'edit', 'form-control form-control-sm selectpicker', $form_contents['speak'], '', '', '');
	                                            }
	                                            else {
	                                                echo $this->Crud_model->select_html('language', 'speak', 'name', 'edit', 'form-control form-control-sm selectpicker', $language[0]['speak'], '', '', '');
	                                            }
	                                        ?>
	                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                                        <div class="help-block with-errors">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-md-6">
	                                    <div class="form-group has-feedback">
	                                        <label for="read" class="text-uppercase c-gray-light"><?php echo translate('read')?></label>
	                                        <?php
	                                            if (!empty($form_contents)) {
	                                                echo $this->Crud_model->select_html('language', 'read', 'name', 'edit', 'form-control form-control-sm selectpicker', $form_contents['read'], '', '', '');
	                                            }
	                                            else {
	                                                echo $this->Crud_model->select_html('language', 'read', 'name', 'edit', 'form-control form-control-sm selectpicker', $language[0]['read'], '', '', '');
	                                            }
	                                        ?>
	                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                                        <div class="help-block with-errors">
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
				            </div>
				        </div>
				        <?php
	                        }
	                    ?>

	                    <?php
	                        if ($this->db->get_where('frontend_settings', array('type' => 'permanent_address'))->row()->value == "yes") {
	                    ?>
				        <div class="panel panel-dark">
				            <div class="panel-heading">
				                <h3 class="panel-title"><?php echo translate('permanent_address')?></h3>
				            </div>
				            <div class="panel-body">
				                <div class='clearfix'>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <div class="form-group has-feedback">
	                                        <label for="permanent_country" class="text-uppercase c-gray-light"><?php echo translate('country')?><span class="text-danger">*</span></label>
	                                        <?php
	                                            if (!empty($form_contents)) {
	                                                echo $this->Crud_model->select_html('country', 'permanent_country', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_country_f_edit', $form_contents['permanent_country'], '', '', '');
	                                            }
	                                            else {
	                                                echo $this->Crud_model->select_html('country', 'permanent_country', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_country_f_edit', $permanent_address[0]['permanent_country'], '', '', '');
	                                            }
	                                        ?>
	                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                                        <div class="help-block with-errors">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-md-6">
	                                    <div class="form-group has-feedback">
	                                        <label for="permanent_state" class="text-uppercase c-gray-light"><?php echo translate('state')?><span class="text-danger">*</span></label>
	                                        <?php
	                                            if (!empty($permanent_address[0]['permanent_country'])) {
	                                                if (!empty($permanent_address[0]['permanent_state'])) {
	                                                    echo $this->Crud_model->select_html('state', 'permanent_state', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_state_f_edit', $permanent_address[0]['permanent_state'], 'country_id', $permanent_address[0]['permanent_country'], '');
	                                                } else {
	                                                    echo $this->Crud_model->select_html('state', 'permanent_state', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_state_f_edit', '', 'country_id', $permanent_address[0]['permanent_country'], '');
	                                                }
	                                            }
	                                            elseif (!empty($form_contents['permanent_country'])){
	                                                if (!empty($form_contents['permanent_state'])) {
	                                                    echo $this->Crud_model->select_html('state', 'permanent_state', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_state_f_edit', $form_contents['permanent_state'], 'country_id', $form_contents['permanent_country'], '');
	                                                } else {
	                                                    echo $this->Crud_model->select_html('state', 'permanent_state', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_state_f_edit', '', 'country_id', $form_contents['permanent_country'], '');
	                                                }
	                                            }
	                                            else {
	                                            ?>
	                                                <select class="form-control form-control-sm selectpicker permanent_state_f_edit" name="permanent_state">
	                                                    <option value=""><?php echo translate('choose_a_country_first')?></option>
	                                                </select>
	                                            <?php
	                                            }
	                                        ?>
	                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                                        <div class="help-block with-errors">
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <div class="form-group has-feedback">
	                                        <label for="permanent_city" class="text-uppercase c-gray-light"><?php echo translate('city')?></label>
	                                        <?php
	                                            if (!empty($permanent_address[0]['permanent_state'])) {
	                                                if (!empty($permanent_address[0]['permanent_city'])) {
	                                                    echo $this->Crud_model->select_html('city', 'permanent_city', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_city_f_edit', $permanent_address[0]['permanent_city'], 'state_id', $permanent_address[0]['permanent_state'], '');
	                                                } else {
	                                                    echo $this->Crud_model->select_html('city', 'permanent_city', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_city_f_edit', '', 'state_id', $permanent_address[0]['permanent_state'], '');
	                                                }
	                                            }
	                                            elseif (!empty($form_contents['permanent_state'])){
	                                                if (!empty($form_contents['permanent_city'])) {
	                                                    echo $this->Crud_model->select_html('city', 'permanent_city', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_city_f_edit', $form_contents['permanent_city'], 'state_id', $form_contents['permanent_state'], '');
	                                                } else {
	                                                    echo $this->Crud_model->select_html('city', 'permanent_city', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_city_f_edit', '', 'state_id', $form_contents['permanent_state'], '');
	                                                }
	                                            }
	                                            else {
	                                            ?>
	                                                <select class="form-control form-control-sm selectpicker permanent_city_f_edit" name="permanent_city">
	                                                    <option value=""><?php echo translate('choose_a_state_first')?></option>
	                                                </select>
	                                            <?php
	                                            }
	                                        ?>
	                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                                        <div class="help-block with-errors">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-md-6">
	                                    <div class="form-group has-feedback">
	                                        <label for="permanent_postal_code" class="text-uppercase c-gray-light"><?php echo translate('postal-Code')?></label>
	                                        <input type="text" class="form-control no-resize" name="permanent_postal_code" value="<?php if(!empty($form_contents)){echo $form_contents['permanent_postal_code'];} else{echo $permanent_address[0]['permanent_postal_code'];}?>">
	                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
	                                        <div class="help-block with-errors">
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
				            </div>
				        </div>
				        <?php
	                        }
	                    ?>

	                    <div class="panel-footer text-center">
							<button class="btn btn-primary btn-sm btn-labeled fa fa-floppy-o" type="submit"><?php echo translate('update')?></button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
