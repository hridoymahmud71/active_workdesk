<?php
if (empty($get_all_projects)) {
    ?>
        <div class='text-center pt-5 pb-5'><i class='fa fa-exclamation-triangle fa-5x'></i><h5><?=translate('no_result_found!')?></h5></div>
    <?php
}
foreach ($get_all_projects as $projects): ?>
    <div class="block block--style-3 list z-depth-1-top" id="block_<?=$projects->project_id?>">
        <div class="block-image">
            <a onclick="return goto_profile()">
                <?php
                if (file_exists('uploads/project_image/'.$projects->atachement)) { ?>
                    <div class="listing-image" style="background-image: url(<?=base_url()?>uploads/project_image/project_1.jpg"></div>
                <?php }else { ?>
                     <div class="listing-image" style="background-image: url(<?=base_url()?>uploads/project_image/default_image.jpg"></div>
                <?php } ?>

               
            </a>
        </div>
        <div class="block-title-wrapper">
            
            <h3 class="heading heading-5 strong-500>">
                <a class="c-base-1"><?=$projects->title?></a>
            </h3>
            <table class="table-striped table-bordered mb-2" style="font-size: 12px;">
                <tr>
                    <td height="30" style="padding-left: 5px;" class="font-dark"><b><?php echo translate('project_id')?></b></td>
                    <td height="30" style="padding-left: 5px;" class="font-dark" colspan="3"><b><?php echo $projects->project_id; ?></b><td>
                </tr>
                <tr>
                   <td width="240" height="30" style="padding-left: 5px;" class="font-dark"><b><?php echo translate('project_type')?></b></td>
                    <td width="240" height="30" style="padding-left: 5px;" class="font-dark">
                        <?php echo $this->Crud_model->get_type_name_by_id('project_type',$projects->project_type , 'name');?> 
                    </td>
                </tr>
                <tr>
                   <td width="240" height="30" style="padding-left: 5px;" class="font-dark"><b><?php echo translate('project_sub_category')?></b></td>
                    <td width="240" height="30" style="padding-left: 5px;" class="font-dark">
                        <?php echo $this->Crud_model->get_type_name_by_id('project_category',$projects->project_category , 'title');?> 
                    </td>
                </tr>
                <tr>
                    <td width="240" height="30" style="padding-left: 5px;" class="font-dark"><b><?php echo translate('project_sub_category')?></b></td>
                    <td width="240" height="30" style="padding-left: 5px;" class="font-dark">
                        <?php echo $this->Crud_model->get_type_name_by_id('project_category',$projects->project_sub_category , 'title');?> 
                    </td>
                </tr>
                <tr>
                    <td width="240" height="30" style="padding-left: 5px;" class="font-dark"><b><?php echo translate('other')?></b></td>
                    <td width="240" height="30" style="padding-left: 5px;" class="font-dark">
                         
                    </td>
                </tr>
            </table>
        </div>
        <div class="block-footer b-xs-top">
            <div class="row align-items-center">
                <div class="col-sm-12 text-center">
                    <ul class="inline-links inline-links--style-3">
                        <li class="listing-hover">
                            <a onclick="return goto_profile()">
                                <i class="fa fa-id-card"></i><?php echo translate('full_profile')?>
                            </a>
                        </li>
                        <li class="listing-hover">
                            <a>
                                <i class="fa fa-id-card"></i><?php echo translate('express_interest')?>
                            </a>
                        </li>
                        <li class="listing-hover">
                            <a>
                                <i class="fa fa-id-card"></i><?php echo translate('short_list')?>
                            </a>
                        </li>
                        <li class="listing-hover">
                            <a>
                                <i class="fa fa-id-card"></i><?php echo translate('follow')?>
                            </a>
                        </li>
                        <li class="listing-hover">
                            <a>
                                <i class="fa fa-id-card"></i><?php echo translate('ignor')?>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
<?php endforeach ?>
<div id="pseudo_pagination" style="display: none;">
    <?= $this->ajax_pagination->create_links();?>
</div>

<script type="text/javascript">
    $('#pagination').html($('#pseudo_pagination').html());
</script>