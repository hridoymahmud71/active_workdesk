<script src="<?=base_url()?>template/front/js/jquery.inputmask.bundle.min.js"></script>
<div class="col-lg-4 size-sm">
    <div class="sidebar">
        <div class="">
            <div class="card">
                <div class="card-title b-xs-bottom">
                    <h3 class="heading heading-sm text-uppercase"><?php echo translate('advanced_search')?></h3>
                </div>
                <div class="card-body">
                    <form class="form-default" id="filter_form" data-toggle="validator" role="form">
                        <div class="row">

                            <div class="col-sm-12">
                                <div class="form-group has-feedback">
                                    <label for="" class="text-uppercase"><?php echo translate('project_type')?></label>
                                    <select name="project_type" class="form-control form-control-sm selectpicker">
                                        <option value=""><?php echo translate('choose_one') ?></option>
                                        <?php 
                                            $projects_data = $this->db->get('project_type')->result(); 
                                            foreach ($projects_data as $projects) { ?>
                                                 <option value="<?php echo $projects->project_type_id ?>"><?php echo $projects->name; ?></option>
                                            <?php }
                                        ?>
                                    </select>
                                    <div class="help-block with-errors">
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <label for="" class="text-uppercase"><?php echo translate('peiject_category')?></label>

                                    <select name="project_category" onchange="project_sub_category_choose(this.value)"class="form-control form-control-sm selectpicker ">
                                        <option value=""><?php echo translate('choose_one') ?></option>
                                        <?php 
                                            $project_category_data = $this->db->get_where("project_category",array('parent'=>0))->result();
                                            foreach ($project_category_data as $project_category) { ?>
                                                 <option value="<?php echo $project_category->project_category_id ?>"><?php echo $project_category->title; ?></option>
                                            <?php }
                                        ?>
                                    </select>

                                    <div class="help-block with-errors">
                                    </div>
                                </div>
                                <div class="form-group has-feedback" id="">
                                    <label for="" class="text-uppercase"><?php echo translate('project_sub_category')?></label>

                                    <select class="form-control form-control-sm selectpicker s_project_sub_category" name="project_sub_category">
                                        <option value=""><?php echo translate('choose_a_project_category_first')?></option>
                                    </select>
                                    <div class="help-block with-errors">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-block btn-base-1 mt-2 z-depth-2-bottom" onclick="filter_projects('0','search')"><?php echo translate('search')?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
