<script src="<?=base_url()?>template/front/js/jquery.inputmask.bundle.min.js"></script>
<?php include_once APPPATH.'views/front/profile/profile_nav.php';?>
<section class="slice sct-color-2">
    <div class="profile">
        <div class="container">
            <?php foreach ($get_client as $client): ?>
                <div class="row cols-md-space cols-sm-space cols-xs-space">
                    <div class="col-lg-4">
                        <?php include_once APPPATH.'views/front/client/left_panel.php';?>
                    </div>
                    <div class="col-lg-8">
                        <?php
                            $basic_info = $this->Crud_model->get_type_name_by_id('client', $this->session->userdata['client_id'], 'basic_info');
                            $basic_info_data = json_decode($basic_info, true);

                            
                            $language = $this->Crud_model->get_type_name_by_id('client', $this->session->userdata['client_id'], 'language');
                            $language_data = json_decode($language, true);


                            $permanent_address = $this->Crud_model->get_type_name_by_id('client', $this->session->userdata['client_id'], 'permanent_address');
                            $permanent_address_data = json_decode($permanent_address, true);

                        ?>
                        <form id="edit_profile_form" class="form-default" role="form" action="<?=base_url()?>home/client_profile/update_all" method="POST">
                            <div class="widget">
                                <div class="card z-depth-2-top" id="profile_load">
                                    <?php if (!empty(validation_errors())): ?>
                                        <div class="widget" id="profile_error">
                                            <div style="border-bottom: 1px solid #e6e6e6;">
                                                <div class="card-title" style="padding: 0.5rem 1.5rem; color: #fcfcfc; background-color: #de1b1b; border-top-right-radius:0.25rem; border-top-left-radius:0.25rem;">You <b>Must Provide</b> the Information(s) bellow</div>
                                                <div class="card-body" style="padding: 0.5rem 1.5rem; background: rgba(222, 27, 27, 0.10);">
                                                    <style>
                                                        #profile_error p {
                                                            color: #DE1B1B !important; margin: 0px !important; font-size: 12px !important;
                                                        }
                                                    </style>
                                                    <?= validation_errors();?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                        endif;
                                    ?>
                                    <div class="card-title">
                                        <h3 class="heading heading-6 strong-500 pull-left">
                                            <b><?php echo translate('edit_profile_informations')?></b>
                                        </h3>
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-base-1 btn-rounded btn-sm btn-shadow"><i class="ion-checkmark"></i> <?php echo translate('update')?></button>
                                            <a href="<?=base_url()?>home/profile" class="btn btn-danger btn-sm btn-shadow"><i class="ion-close"></i> <?php echo translate('cancel')?></a>
                                        </div>
                                    </div>
                                    <div class="card-body" style="padding: 1.5rem 0.5rem;">
                                        <div class="feature feature--boxed-border feature--bg-1 pt-3 pb-0 pl-3 pr-3 mb-3 border_top2x">
                                            <div id="edit_introduction">
                                                <div class="card-inner-title-wrapper  pt-0">
                                                    <h3 class="card-inner-title pull-left">
                                                    <?php echo translate('introduction')?> </h3>
                                                </div>
                                                <div class='clearfix'>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group has-feedback">
                                                            <textarea name="introduction" class="form-control no-resize" rows="6"><?php if(!empty($form_contents)){echo $form_contents['introduction'];} else{echo $client->introduction;}?></textarea>
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <div class="help-block with-errors">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="feature feature--boxed-border feature--bg-1 pt-3 pb-0 pl-3 pr-3 mb-3 border_top2x">
                                            <div id="edit_basic_info">
                                                <div class="card-inner-title-wrapper  pt-0">
                                                    <h3 class="card-inner-title pull-left">
                                                    <?php echo translate('basic_information')?> </h3>
                                                </div>
                                                <div class='clearfix'>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <label for="first_name" class="text-uppercase c-gray-light"><?php echo translate('first_name')?></label>
                                                            <input type="text" class="form-control no-resize" name="first_name" value="<?php if(!empty($form_contents)){echo $form_contents['first_name'];} else{echo $client->first_name;}?>">
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <div class="help-block with-errors">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <label for="last_name" class="text-uppercase c-gray-light"><?php echo translate('last_name')?></label>
                                                            <input type="text" class="form-control no-resize" name="last_name" value="<?php if(!empty($form_contents)){echo $form_contents['last_name'];} else{echo $client->last_name;}?>">
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <div class="help-block with-errors">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <label for="gender" class="text-uppercase c-gray-light"><?php echo translate('gender')?></label>
                                                            <?php
                                                                if (!empty($form_contents)) {
                                                                    echo $this->Crud_model->select_html('gender', 'gender', 'name', 'edit', 'form-control form-control-sm selectpicker', $form_contents['gender'], '', '', '');
                                                                }
                                                                else {
                                                                    echo $this->Crud_model->select_html('gender', 'gender', 'name', 'edit', 'form-control form-control-sm selectpicker', $client->gender, '', '', '');
                                                                }
                                                            ?>
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <div class="help-block with-errors">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <label for="email" class="text-uppercase c-gray-light"><?php echo translate('email')?></label>
                                                            <input type="hidden" name="old_email" value="<?=$client->email?>">
                                                            <input type="email" class="form-control no-resize" name="email" value="<?php if(!empty($form_contents)){echo $form_contents['email'];} else{echo $client->email;}?>">
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <div class="help-block with-errors">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <label for="date_of_birth" class="text-uppercase c-gray-light"><?php echo translate('date_of_birth')?></label>
                                                            <input type="date" class="form-control no-resize" name="date_of_birth" value="<?php if(!empty($form_contents)){echo $form_contents['date_of_birth'];} else{echo date('Y-m-d', $client->date_of_birth);}?>">
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <div class="help-block with-errors">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <label for="mobile" class="text-uppercase c-gray-light"><?php echo translate('mobile')?></label>
                                                            <input type="hidden" name="old_mobile" value="<?=$get_client[0]->mobile?>">
                                                            <input type="number" class="form-control no-resize" name="mobile" value="<?=$get_client[0]->mobile?>">
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <?php
                                            if ($this->db->get_where('frontend_settings', array('type' => 'language'))->row()->value == "yes") {
                                        ?>
                                        <div class="feature feature--boxed-border feature--bg-1 pt-3 pb-0 pl-3 pr-3 mb-3 border_top2x">
                                            <div id="edit_language">
                                                <div class="card-inner-title-wrapper  pt-0">
                                                    <h3 class="card-inner-title pull-left">
                                                    <?php echo translate('language')?> </h3>
                                                </div>
                                                <div class='clearfix'>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <label for="mother_tongue" class="text-uppercase c-gray-light"><?php echo translate('mother_tongue')?></label>
                                                            <?php
                                                                if (!empty($form_contents)) {
                                                                    echo $this->Crud_model->select_html('language', 'mother_tongue', 'name', 'edit', 'form-control form-control-sm selectpicker', $form_contents['mother_tongue'], '', '', '');
                                                                }
                                                                else {
                                                                    echo $this->Crud_model->select_html('language', 'mother_tongue', 'name', 'edit', 'form-control form-control-sm selectpicker', $language_data[0]['mother_tongue'], '', '', '');
                                                                }
                                                            ?>
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <div class="help-block with-errors">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <label for="language" class="text-uppercase c-gray-light"><?php echo translate('language')?></label>
                                                            <?php
                                                                if (!empty($form_contents)) {
                                                                    echo $this->Crud_model->select_html('language', 'language', 'name', 'edit', 'form-control form-control-sm selectpicker', $form_contents['language'], '', '', '');
                                                                }
                                                                else {
                                                                    echo $this->Crud_model->select_html('language', 'language', 'name', 'edit', 'form-control form-control-sm selectpicker', $language_data[0]['language'], '', '', '');
                                                                }
                                                            ?>
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <div class="help-block with-errors">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <label for="speak" class="text-uppercase c-gray-light"><?php echo translate('speak')?></label>
                                                            <?php
                                                                if (!empty($form_contents)) {
                                                                    echo $this->Crud_model->select_html('language', 'speak', 'name', 'edit', 'form-control form-control-sm selectpicker', $form_contents['speak'], '', '', '');
                                                                }
                                                                else {
                                                                    echo $this->Crud_model->select_html('language', 'speak', 'name', 'edit', 'form-control form-control-sm selectpicker', $language_data[0]['speak'], '', '', '');
                                                                }
                                                            ?>
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <div class="help-block with-errors">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <label for="read" class="text-uppercase c-gray-light"><?php echo translate('read')?></label>
                                                            <?php
                                                                if (!empty($form_contents)) {
                                                                    echo $this->Crud_model->select_html('language', 'read', 'name', 'edit', 'form-control form-control-sm selectpicker', $form_contents['read'], '', '', '');
                                                                }
                                                                else {
                                                                    echo $this->Crud_model->select_html('language', 'read', 'name', 'edit', 'form-control form-control-sm selectpicker', $language_data[0]['read'], '', '', '');
                                                                }
                                                            ?>
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <div class="help-block with-errors">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                            }
                                        ?>
                                        <?php
                                            if ($this->db->get_where('frontend_settings', array('type' => 'permanent_address'))->row()->value == "yes") {
                                        ?>
                                        <div class="feature feature--boxed-border feature--bg-1 pt-3 pb-0 pl-3 pr-3 mb-3 border_top2x">
                                            <div id="edit_nentnent_address">
                                                <div class="card-inner-title-wrapper  pt-0">
                                                    <h3 class="card-inner-title pull-left">
                                                    <?php echo translate('permanent_address')?> </h3>
                                                </div>
                                                <div class='clearfix'>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <label for="permanent_country" class="text-uppercase c-gray-light"><?php echo translate('country')?></label>
                                                            <?php
                                                                if (!empty($form_contents)) {
                                                                    echo $this->Crud_model->select_html('country', 'permanent_country', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_country_f_edit', $form_contents['permanent_country'], '', '', '');
                                                                }
                                                                else {
                                                                    echo $this->Crud_model->select_html('country', 'permanent_country', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_country_f_edit', $permanent_address_data[0]['permanent_country'], '', '', '');
                                                                }
                                                            ?>
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <div class="help-block with-errors">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <label for="permanent_state" class="text-uppercase c-gray-light"><?php echo translate('state')?></label>
                                                            <?php
                                                                if (!empty($permanent_address_data[0]['permanent_country'])) {
                                                                    if (!empty($permanent_address_data[0]['permanent_state'])) {
                                                                        echo $this->Crud_model->select_html('state', 'permanent_state', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_state_f_edit', $permanent_address_data[0]['permanent_state'], 'country_id', $permanent_address_data[0]['permanent_country'], '');
                                                                    } else {
                                                                        echo $this->Crud_model->select_html('state', 'permanent_state', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_state_f_edit', '', 'country_id', $permanent_address_data[0]['permanent_country'], '');
                                                                    }
                                                                }
                                                                elseif (!empty($form_contents['permanent_country'])){
                                                                    if (!empty($form_contents['permanent_state'])) {
                                                                        echo $this->Crud_model->select_html('state', 'permanent_state', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_state_f_edit', $form_contents['permanent_state'], 'country_id', $form_contents['permanent_country'], '');
                                                                    } else {
                                                                        echo $this->Crud_model->select_html('state', 'permanent_state', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_state_f_edit', '', 'country_id', $form_contents['permanent_country'], '');
                                                                    }
                                                                }
                                                                else {
                                                                ?>
                                                                    <select class="form-control form-control-sm selectpicker permanent_state_f_edit" name="permanent_state">
                                                                        <option value=""><?php echo translate('choose_a_country_first')?></option>
                                                                    </select>
                                                                <?php
                                                                }
                                                            ?>
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <div class="help-block with-errors">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <label for="permanent_city" class="text-uppercase c-gray-light"><?php echo translate('city')?></label>
                                                            <?php
                                                                if (!empty($permanent_address_data[0]['permanent_state'])) {
                                                                    if (!empty($permanent_address_data[0]['permanent_city'])) {
                                                                        echo $this->Crud_model->select_html('city', 'permanent_city', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_city_f_edit', $permanent_address_data[0]['permanent_city'], 'state_id', $permanent_address_data[0]['permanent_state'], '');
                                                                    } else {
                                                                        echo $this->Crud_model->select_html('city', 'permanent_city', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_city_f_edit', '', 'state_id', $permanent_address_data[0]['permanent_state'], '');
                                                                    }
                                                                }
                                                                elseif (!empty($form_contents['permanent_state'])){
                                                                    if (!empty($form_contents['permanent_city'])) {
                                                                        echo $this->Crud_model->select_html('city', 'permanent_city', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_city_f_edit', $form_contents['permanent_city'], 'state_id', $form_contents['permanent_state'], '');
                                                                    } else {
                                                                        echo $this->Crud_model->select_html('city', 'permanent_city', 'name', 'edit', 'form-control form-control-sm selectpicker permanent_city_f_edit', '', 'state_id', $form_contents['permanent_state'], '');
                                                                    }
                                                                }
                                                                else {
                                                                ?>
                                                                    <select class="form-control form-control-sm selectpicker permanent_city_f_edit" name="permanent_city">
                                                                        <option value=""><?php echo translate('choose_a_state_first')?></option>
                                                                    </select>
                                                                <?php
                                                                }
                                                            ?>
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <div class="help-block with-errors">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <label for="permanent_postal_code" class="text-uppercase c-gray-light"><?php echo translate('postal-Code')?></label>
                                                            <input type="text" class="form-control no-resize" name="permanent_postal_code" value="<?php if(!empty($form_contents)){echo $form_contents['permanent_postal_code'];} else{echo $permanent_address_data[0]['permanent_postal_code'];}?>">
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <div class="help-block with-errors">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                            }
                                        ?>
                                        <div class="col-8 ml-auto mr-auto">
                                            <div class="row text-center">
                                                <div class="col-6">
                                                    <a href="<?=base_url()?>home/profile" class="btn btn-danger btn-sm z-depth-2-bottom" style="width: 100%"><?php echo translate('cancel')?></a>
                                                </div>
                                                <div class="col-6">
                                                    <button type="submit" class="btn btn-base-1 btn-rounded btn-sm z-depth-2-bottom btn-icon-only" style="width: 100%"><?php echo translate('update')?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
        $(".height_mask").inputmask({
            mask: "9.99",
            greedy: false,
            definitions: {
                '*': {
                    validator: "[0-9]"
                }
            }
        });
    });
</script>
<script>
    $(".present_country_f_edit").change(function(){
        var country_id = $(".present_country_f_edit").val();
        if (country_id == "") {
            $(".present_state_f_edit").html("<option value=''><?php echo translate('choose_a_country_first')?></option>");
            $(".present_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");
        } else {
            $.ajax({
                url: "<?=base_url()?>home/get_dropdown_by_id/state/country_id/"+country_id, // form action url
                type: 'POST', // form submit method get/post
                dataType: 'html', // request type html/json/xml
                cache       : false,
                contentType : false,
                processData : false,
                success: function(data) {
                    $(".present_state_f_edit").html(data);
                    $(".present_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");
                },
                error: function(e) {
                    console.log(e)
                }
            });
        }
    });
    $(".present_state_f_edit").change(function(){
        var state_id = $(".present_state_f_edit").val();
        if (state_id == "") {
            $(".present_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");
        } else {
            $.ajax({
                url: "<?=base_url()?>home/get_dropdown_by_id/city/state_id/"+state_id, // form action url
                type: 'POST', // form submit method get/post
                dataType: 'html', // request type html/json/xml
                cache       : false,
                contentType : false,
                processData : false,
                success: function(data) {
                    $(".present_city_f_edit").html(data);
                },
                error: function(e) {
                    console.log(e)
                }
            });
        }
    });

    $(".permanent_country_f_edit").change(function(){
        var country_id = $(".permanent_country_f_edit").val();
        if (country_id == "") {
            $(".permanent_state_f_edit").html("<option value=''><?php echo translate('choose_a_country_first')?></option>");
            $(".permanent_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");
        } else {
            $.ajax({
                url: "<?=base_url()?>home/get_dropdown_by_id/state/country_id/"+country_id, // form action url
                type: 'POST', // form submit method get/post
                dataType: 'html', // request type html/json/xml
                cache       : false,
                contentType : false,
                processData : false,
                success: function(data) {
                    $(".permanent_state_f_edit").html(data);
                    $(".present_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");
                },
                error: function(e) {
                    console.log(e)
                }
            });
        }
    });
    $(".permanent_state_f_edit").change(function(){
        var state_id = $(".permanent_state_f_edit").val();
        if (state_id == "") {
            $(".permanent_city_f_edit").html("<option value=''><?php echo translate('choose_a_state_first')?></option>");
        } else {
            $.ajax({
                url: "<?=base_url()?>home/get_dropdown_by_id/city/state_id/"+state_id, // form action url
                type: 'POST', // form submit method get/post
                dataType: 'html', // request type html/json/xml
                cache       : false,
                contentType : false,
                processData : false,
                success: function(data) {
                    $(".permanent_city_f_edit").html(data);
                },
                error: function(e) {
                    console.log(e)
                }
            });
        }
    });
    $(".prefered_country_f_edit").change(function(){
        var country_id = $(".prefered_country_f_edit").val();
        if (country_id == "") {
            $(".prefered_state_f_edit").html("<option value=''><?php echo translate('choose_a_country_first')?></option>");
        } else {
            $.ajax({
                url: "<?=base_url()?>home/get_dropdown_by_id/state/country_id/"+country_id, // form action url
                type: 'POST', // form submit method get/post
                dataType: 'html', // request type html/json/xml
                cache       : false,
                contentType : false,
                processData : false,
                success: function(data) {
                    $(".prefered_state_f_edit").html(data);
                },
                error: function(e) {
                    console.log(e)
                }
            });
        }
    });


   
</script>
