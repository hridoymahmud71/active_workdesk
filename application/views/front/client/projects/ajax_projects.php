<table class="table table-sm table-striped table-hover table-bordered table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>
                <?php echo translate('title')?>
            </th>
            <th>
                <?php echo translate('project_type')?>
            </th>
            <th>
                <?php echo translate('project_category')?>
            </th>
            <th>
                <?php echo translate('project_sub_categoey')?>
            </th>
            <th>
                <?php echo translate('options')?>
            </th>
        </tr>
        </thead>
        <tbody>
            <?php $i=$page; ?>
            <?php foreach ($projects_info as $project_info): ?>
                <tr>
                    <td><?=$i+1?></td>
                    <td><?php echo $project_info->title; ?></td>
                    <td><?php echo $this->db->get_where('project_type',array('project_type_id'=>$project_info->project_type))->row()->name; ?></td>
                    <td><?php echo $this->db->get_where('project_category',array('project_category_id'=>$project_info->project_category))->row()->title; ?></td>
                    <td><?php echo $this->db->get_where('project_category',array('project_category_id'=>$project_info->project_sub_category))->row()->title; ?></td>
                    <td>
                        <button type="button" class="btn btn-base-1 btn-sm btn-icon-only btn-shadow mb-1" > <i class="fa fa-pencil"></i> <?=translate('edit')?>
                        </button>
                         <button type="button" class="btn btn-danger btn-sm btn-icon-only btn-shadow mb-1" > <i class="fa fa-trash"></i> <?=translate('delete')?>
                        </button>
                    </td>
                </tr>
                <?php $i++; ?>
            <?php endforeach ?>
        </tbody>
    </table>


    <div id="pseudo_pagination" style="display: none;">
    <?= $this->ajax_pagination->create_links();?>
</div>
<script type="text/javascript">
    $('#pagination').html($('#pseudo_pagination').html());
</script>